local S = minetest.get_translator("draniki")

minetest.register_tool("draniki:grater", {
    description = S("Grater"),
    inventory_image = "draniki_grater.png",
    stack_max = 1,
    _repair_material = "mcl_core:iron_nugget"
})

minetest.register_craft({
    output = "draniki:grater",
    recipe = {
        {"mcl_core:iron_ingot", "mcl_core:iron_nugget", "mcl_core:iron_ingot"},
        {"mcl_core:iron_ingot", "mcl_core:iron_nugget", "mcl_core:iron_ingot"},
        {"mcl_core:iron_ingot", "mcl_core:iron_nugget", "mcl_core:iron_ingot"}
    }
})

minetest.register_craftitem("draniki:grated_potatoes", {
    description = S("Grated potatoes"),
    inventory_image = "draniki_grated_potatoes.png",
    stack_max = 4,
    _mcl_saturation = 1,
    on_secondary_use = minetest.item_eat(1)
})

minetest.register_craft({
    type = "shapeless",
    output = "draniki:grated_potatoes",
    recipe = {"draniki:grater", "mcl_farming:potato_item"},
})

function find_grater_index(craft_grid)
    for i = 1, #craft_grid do
        if craft_grid[i]:get_name() == "draniki:grater" then
            return i
        end
    end
end

minetest.register_on_craft(function(itemstack, player, old_craft_grid, craft_inv)
    grater_i = find_grater_index(old_craft_grid)
    if not grater_i then return end
    grater_item_stack = old_craft_grid[grater_i]
    grater_item_stack:set_wear(grater_item_stack:get_wear() + 6553)
    craft_inv:set_stack("craft", grater_i, grater_item_stack)
end)

minetest.register_craftitem("draniki:dranik", {
    description = S("Potato pancake"),
    inventory_image = "draniki_dranik.png",
    stack_max = 4,
    _mcl_saturation = 10.0,
    groups = {food = 2, eatable = 10, compostability = 100},
    on_secondary_use = minetest.item_eat(10)
})

minetest.register_craft({
    type = "cooking",
    output = "draniki:dranik",
    recipe = "draniki:grated_potatoes",
    cooktime = 10.0
})
